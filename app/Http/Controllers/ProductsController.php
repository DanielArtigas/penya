<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Cathegory;

class ProductsController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
  {
       $products = Product::paginate(10);

         $cathegories = Cathegory::all();
        return view('products.index', ['products' => $products],['cathegories'=>$cathegories]);

    }


    public function create()
    {
        $cathegories =Cathegory::all();
        return view('products.create', ['cathegories'=>$cathegories]);

    }


  public function store(Request $request)
    {

        $reglas = [
            'name' => 'required|max:255|min:3',
            'price' => 'required|numeric',
            'cathegories_id' => 'min:1'
        ];
        $request->validate($reglas);
        $product = new Product();
        $product->fill($request->all());
        $product->save();
        return redirect('/products');
    }

    public function show($id)
    {
        $products = Product::findOrFail($id);
        $cathegories = Cathegory::all();
        return view('products.show',['products'=>$products],['cathegories'=>$cathegories]);

    }

    public function edit($id)
    {
        $products = Product::findOrFail($id);
          $cathegories = Cathegory::all();
        return view('products.edit', ['products' => $products],['cathegories'=>$cathegories]);
    }

    public function update(Request $request, $id)
    {

        $reglas = [
            'name'=> 'required|max:255|min:3',
            'price'=> 'required|numeric',
            'cathegory_id'=> 'min:1'
        ];
        $request->validate($reglas);
        $product = Product::findOrFail($id);
        $product->fill($request->all());
        $product->save();
        return redirect('/products');
    }

    public function destroy($id)
    {

        Product::destroy($id);

        return back();
    }

     public function especial()
    {
        $products = Product::where('id', '>=', 15)
            ->where('id', '<=', 20)
            ->get();

        dd($products);
        return "especial";
        return redirect('/products');
        // return "Especial";
    }
}
