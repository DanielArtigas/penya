<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cathegory;
use App\Product;

class CathegoriesController extends Controller
{
  public function __construct()
  {
    $cathegories = Cathegory::paginate(10);

    $this->middleware('auth')->except('index');


}

public function index()
{
    $cathegories = Cathegory::paginate(10);
    $product = Product::all();
    return view('cathegories.index',['cathegories'=>$cathegories], ['product' => $product]);
}


public function create()
{
    return view('cathegories.create');
}


public function store(Request $request)
{
    $reglas = [
        'name' => 'required|max:255|min:3'
    ];
    $request->validate($reglas);
    $category = new Cathegory();
    $category->fill($request->all());
    $category->save();
    return redirect('/cathegories');
}


public function show($id)
{

  $cathegory = Cathegory::findOrFail($id);
  $product = Product::all();
  return view('cathegories.show', [
    'cathegories' => $cathegory
],['product'=>$product]);

}


public function edit($id)
{

    $cathegory = Cathegory::findOrFail($id);
    $product = Product::all();
    return view('cathegories.edit',['cathegories'=>$cathegory], ['product' => $product]);
}


public function update(Request $request, $id)
{
   $reglas = [
    'name'=> 'required|max:255|min:3'
];
$request->validate($reglas);
$categoria = Cathegory::findOrFail($id);
$categoria->fill($request->all());
$categoria->save();
return redirect('/cathegories');
}


public function destroy($id)
{
    Cathegory::destroy($id);
    return back();
}
}
