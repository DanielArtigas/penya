<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Session;

class GroupController extends Controller
{
    public function index(Request $request)
    {
        $group = $request->session()->get('group');
        if($group==null){
            $group=[];
        }
        return view('group.index', ['users' => $group]);

        // dd($request->session()->get('group'));
    }

    public function flush(Request $request){
        $request->session()->forget('group');
        return back();
    }
    public function addUser(Request $request, $id){
        $user = User::findOrFail($id);

        $group = $request->session()->get('group');

        if($group == null){
            $group = array();
        }


        foreach($group as $user){
             if($user->id == $user->id){
                $user->cantidad++;
                return redirect('/groups');
             }
        }
        $user->cantidad++;
        $request->session()->push('group', $user);

        return redirect('/groups');
        // $group = $request->session('group');
        // if (! $group){
        //     $group = array();
        //     session(['group' => $group]);
        // }
        // $group[] = $user;
    }
}
