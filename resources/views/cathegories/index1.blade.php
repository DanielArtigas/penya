@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row justify-content-center">
    <div class="col-md-8">
      <div class="card">
        <div class="card-header">lista de las categorías<br>

          <a href="/cathegories/create">crear nueva categoría</a>

          <div class="card-body">
           <table class="table">
            <tr>
              <td>nombre</td>

              <td>id</td>
            </tr>
            @forelse($cathegories as $categoria)
            <tr>
              <td>{{$categoria->name}}</td>
              <td>{{$categoria->id}}</td>
              <td>
            </form>

              </td>
            </tr>

            @empty
            <h1>no hay categorias que mostrar</h1>

            @endforelse

          </table>


          <br>
        </div>

      </div>
    </div>
  </div>
</div>
</div>

@endsection
