@extends('layouts.app')

@section('content')
<div class="container">
  <div class="row content">
    <div class="col-sm-12 text-left">
      <h1>Grupo de Usuarios</h1>
      <div class="alert">

      </div>

      <table  class="table table-striped table-hover">
        <thead>
          <tr>
            <th>Nombre</th>
            <th>Email</th>
            <th>Role</th>
            <th>Cantidad</th>

          </tr>
        </thead>


        <tbody>


          @forelse ($users as $user)
          <tr>
            <td>{{ $user->name }}</td>
            <td>{{ $user->email }}</td>
            <td>{{ $user->role->name }}</td>
            <td>{{ $user->cantidad }}</td>
            <td>


        @empty
        <tr><td colspan="4">No hay usuarios!!</td></tr>
        @endforelse
      </tbody>
    </table>

<a class="btn btn-danger"  role="button" href="/groups/flush">
               Limpiar lista
              </a>
  </div>
</div>
</div>
@endsection
