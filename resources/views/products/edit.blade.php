@extends('layouts.app')

@section('title', 'Productos')

@section('content')
    <h1>Editar producto</h1>

    <form method="post" action="/products/{{ $products->id }}">
        {{ csrf_field() }}

        <input type="hidden" name="_method" value="PUT">

        <label>Nombre</label>
        <input type="text" name="name"
        value="{{ old('name') ? old('name') : $products->name }}">
        <div class="alert alert-danger">
            {{ $errors->first('name') }}
        </div>
        <br>
          <label>Precio</label>
        <input type="number" name="price"
        value="{{ old('price') ? old('price') : $products->price }}">
        <div class="alert alert-danger">
            {{ $errors->first('price') }}
        </div>
        <br>

         <br>
       <label> Categoría: </label>
        <select name="cathegory_id">
          @foreach ($cathegories as $cathegories)
           <option value="{{ $cathegories->id }}"
           {{ old('cathegories_id') == $cathegories?
            'selected="selected"' :'' }}>{{ $cathegories->name }}
            </option>
           @endforeach

          <div class="alert alert-danger">
          {{ $errors->first('cathegories_id') }}
         </div>
       </select>

       <br>

        <input type="submit" value="Guardar Cambios">
    </form>
@endsection
