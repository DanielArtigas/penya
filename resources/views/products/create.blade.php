@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Crear producto<br>
                    <div class="card-body">
                        <form method="post" action="/products">
                            {{ csrf_field()}}
                            <label>Nombre:</label>
                            <input type="text" name="name" value=" {{old('name')}} ">
                            <div class="alert alert-danger">
                                {{ $errors->first('name')}}
                            </div>

                            <label>precio:</label>
                            <input type="text" name="price" value=" {{old('price')}} ">
                            <div class="alert alert-danger">
                                {{ $errors->first('name')}}
                            </div>

                            <label> Categoría: </label>
                            <select name="cathegory_id">
                                @foreach ($cathegories as $categoria)
                                <option value="{{ $categoria->id }}"
                                    {{ old('cathegories_id') == $categoria->id ?
                                    'selected="selected"' :
                                    ''
                                }}>{{ $categoria->name }}
                            </option>
                            @endforeach
                            <div class="alert alert-danger">
                                {{ $errors->first('cathegories_id') }}
                            </div>
                        </select>
                        <br>
                        @if(!count($cathegories)==0)
                        <input type="submit" value="Crear">
                        @else
                        <div class="alert alert-danger">Crea una categoria
                            <br>
                            <a href="/cathegories/create">Crear categoria</a>
                        </div>
                        @endif

                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
    @endsection

